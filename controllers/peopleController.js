const fetch = require('node-fetch');
const peopleController = {};

function comparatorSelector(arr, queryParam) {
  if (queryParam === 'name') {
    return sortByName(arr);
  }
  else if (queryParam === 'height') {
    return sortByHeight(arr)
  }
  else if (queryParam === 'mass') {
    return sortByMass(arr)
  }
  else throw new Error('Param must be name, height or mass and it was not.')
}

function sortByHeight(arr) {
  arr.sort(function (a, b) {
    if (Number(a.height) < Number(b.height) || (a.height !== 'unknown' && b.height === 'unknown')) {
      return -1;
    }
    if (Number(a.height) > Number(b.height) || (a.height === 'unknown' && b.height !== 'unknown')) {
      return 1;
    }
    return 0;
  });
  return arr;
}

function sortByMass(arr) {
  arr.sort(function (a, b) {
    if (Number(a.mass.replace(/,/g, '')) < Number(b.mass.replace(/,/g, '')) || (a.mass !== 'unknown' && b.mass === 'unknown')) {
      return -1;
    }
    if (Number(a.mass.replace(/,/g, '')) > Number(b.mass.replace(/,/g, '')) || (a.mass === 'unknown' && b.mass !== 'unknown')) {
      return 1;
    }
    return 0;
  });
  return arr;
}

function sortByName(arr) {
  arr.sort(function (a, b) {
    if (a.name.toLowerCase() < b.name.toLowerCase()) {
      return -1;
    }
    if (a.name.toLowerCase() > b.name.toLowerCase()) {
      return 1;
    }
    return 0;
  });
  return arr
}

peopleController.getPeople = async (req, res, next) => {

  let num = 1;
  let result = [];
  let pageNext = '';

  try {

    while (pageNext !== null) {
      const url = `https://swapi.dev/api/people/?page=${num}`
      const response = await fetch(url);
      const page = await response.json();
      pageNext = page.next;

      page.results.forEach((person) => {
        result.push(person);
      })

      num++

      if (pageNext === null) {
        if (req.query.sortBy) {
          res.locals.people = comparatorSelector(result, req.query.sortBy);
          return next();
        } else {
          res.locals.people = result;
          return next();
        }
      }
    }

  } catch (err) {
    console.log('Error in receiving people data from SWAPI');
    console.log(`status code is ${err.status}`)
    return next({
      log: `Function name: getPeople,\n ${err}`,
      message: { err: 'peopleController.getPeople: ERROR: Check server logs for details' }
    });
  };
};

module.exports = peopleController;