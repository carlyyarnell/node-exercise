const fetch = require('node-fetch');

const planetsController = {};

//input: array of objects where each object has a residents property that is an array of urls
//output: array of objects where each object has a residents property that is an array of names
async function residentNames(arr){
  let arrayOfPlanetsWithNamesNotUrls = []

  for(let i = 0; i < arr.length; i++) {
    const arrayOfUrls = arr[i].residents;
    if(arrayOfUrls.length >= 1){
      const arrayOfNames = []
      arrayOfUrls.forEach(async (url) => {
        const response = await fetch(url);
        const person = await response.json()
        arrayOfNames.push(person.name);
      })
      arr[i].residents = arrayOfNames;
      arrayOfPlanetsWithNamesNotUrls.push(arr[i]);
    }
    else arrayOfPlanetsWithNamesNotUrls.push(arr[i]);
  }
  return arrayOfPlanetsWithNamesNotUrls;
}

planetsController.getPlanets = async (req, res, next) => {

  let num = 1;
  let result = [];
  let pageNext = '';

  try {
    while (num >= 1){
      const url = `https://swapi.dev/api/planets/?page=${num}`
      const response = await fetch(url);
      const page = await response.json();
      console.log(page);
      
      const arrayOfPlanets = await residentNames(page.results);

      result = result.concat(arrayOfPlanets);

      page.next === null ? num = 0 : num++
    }

    res.locals.planets = result;
    return next();

  } catch(err) {
    console.log(`Request to /planets failed because ${err.message} with status code: ${err.status}.`)
    return next({ 
      status: `${err.status}`,
      message: { err: `planetsController.getPlanet encountered an error because: ${err.message}`}
    });
  };
};

module.exports = planetsController;