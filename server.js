const path = require('path');
const express = require('express');

const app = express();
const PORT = 3000;

/**
 * require routers
 */
 const peopleRouter = require('./routes/people.js');
 const planetsRouter = require('./routes/planets.js');
/**
 * handle parsing request body
 */
app.use(express.json());

/**
 * define route handlers
 */
 app.use('/people', peopleRouter);
 app.use('/planets', planetsRouter);
 
app.use('*', (req, res) => {
  //res.sendStatus(404)
  console.log('404: Client attempted access to unknown route');
  res.status(404).send('Sorry can\'t find that!');
});

app.use((err, req, res, next) => {
  const defaultErr = {
    log: 'Express error handler caught unknown middleware error',
    status: 400,
    message: { err: 'An error occurred' },
  };
  const errorObj = Object.assign(defaultErr, err);
  console.log(errorObj.log);
  return res.status(errorObj.status).json(errorObj.message);
});

app.listen(PORT, () => {
  console.log(`Server listening on port: ${PORT}...`);
});

module.exports = app;