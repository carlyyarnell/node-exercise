const express = require('express');

const peopleController = require('../controllers/peopleController');
const router = express.Router();

router.get('/', peopleController.getPeople, (req, res) => {
  return res.status(200).json({ people: res.locals.people });
});

module.exports = router;