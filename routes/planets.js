const express = require('express');

const planetsController = require('../controllers/planetsController');

const router = express.Router();

router.get('/', planetsController.getPlanets, (req, res) => {
  return res.status(200).json({ planets: res.locals.planets });
});

module.exports = router;